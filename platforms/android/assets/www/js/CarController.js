angular.module( 'starter')

    .controller('CarsController', function ($scope, $rootScope,  $ionicModal, $http, $cordovaContacts, $ionicLoading, $state, $ionicPopup) {
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        $scope.newCar = function () {
            if(!localStorage.getItem('purchase') && $scope.carsCount == 2 ){
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Go Pro',
                    template: 'Add unlimited cars and emergency contacts for just $0.99?'
                });
                confirmPopup.then(function(res) {
                    if(res) {
                        $scope.goPro();
                    } else {
                        console.log('You are not sure');
                    }
                });
            }else{
                $ionicModal.fromTemplateUrl('templates/modals/new_car_modal.html', {
                    scope: $scope,
                    animation: 'slide-in-up'
                }).then(function (modal) {
                    $scope.modal = modal;
                    $scope.modal.show();
                });
            }
        };

        $scope.makeCar = function (car) {
            var myEmail = window.localStorage.getItem('email');
            var myGcmid = window.localStorage.getItem('regid');
            car.email = myEmail;
            car.gcmid = myGcmid;
            $scope.postCar(car);
        };
        $scope.postCar = function (car) {
            console.log(JSON.stringify(car));
            $http.post("http://104.236.200.201/cars", JSON.stringify(car)).
                success(function (data, status, headers, config) {
                    $scope.closeModal();
                    $scope.refreshData();
                    //alert(data);
                }).
                error(function (data, status, headers, config) {
                    alert("error " + status);
                });
        };
        $scope.refreshData = function () {
            $ionicLoading.show({
                template: 'Getting Cars...'
            });
            var email = window.localStorage.getItem('email');
            $rootScope.email = email;
            $http.get('http://104.236.200.201/cars/' + email).
                success(function (data, status, headers, config) {
                    $rootScope.cars = data;
                    $scope.carsCount = data.length;
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                }).
                error(function (data, status, headers, config) {
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                });
        };
        $scope.editCar = function (car) {
            $rootScope.$emit('setTheCar', car);
            $state.go("editcar");
        };

        $scope.goPro = function(){
            if((window.device && device.platform == "Android") && typeof inappbilling !== "undefined") {
                inappbilling.buy(function(data) {
                        alert("Thank you for your purchase!");
                        inappbilling.getPurchases(function(result) {
                                if(result != '' || result != []){
                                    window.localStorage.setItem('purchase', JSON.stringify(result[0].productId));
                                }
                            },
                            function(errorPurchases) {
                                //alert("PURCHASE ERROR -> " + errorPurchases);
                            });
                        $scope.refreshData();
                    }, function(errorBuy) {
                        alert("ERROR BUYING -> " + errorBuy);
                    },
                    "pro_contacts");
            }
        };
        $scope.refreshData();

    });



