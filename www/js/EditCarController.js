angular.module('starter').controller('EditCarController', function($scope, $http, $state){


    $scope.editCar = function(editThisCar){
        $http.put('http://104.236.200.201/cars/'+editThisCar.id, JSON.stringify(editThisCar)).
        success(function(d,s){
                $state.go("tab.cars");
            }).
        error(function(d,s){
                alert(s);
            });
    };
});
