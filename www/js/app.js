
angular.module('starter', ['ionic',
    'ngCordova',
    'app.login',
    'app.register'
])

.run(function($ionicPlatform,  $cordovaPush, $rootScope, $state) {
          $ionicPlatform.ready(function() {
              if((window.device && device.platform == "Android") && typeof inappbilling !== "undefined") {
                  inappbilling.init(function(resultInit) {
                          //alert("IAB Initialized");
                          inappbilling.getPurchases(function(result) {
                                  if(result != '' || result != []){
                                      window.localStorage.setItem('purchase', JSON.stringify(result[0].productId));
                                  }
                              },
                              function(errorPurchases) {
                                  //alert("PURCHASE ERROR -> " + errorPurchases);
                              });
                      },
                      function(errorInit) {
                          alert("ERROR -> " + errorInit);
                      },
                      {showLog: true},
                      ["pro_contacts"]);
              }

            if (window.cordova && window.cordova.plugins.Keyboard) {
              cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
              StatusBar.styleDefault();
            }
              $state.go("tab.cars");
              $rootScope.$on('setTheCar', function (event, data) {
                  $rootScope.editThisCar =data;
              });
              $rootScope.$on('setTheContact', function (event, data) {
                  $rootScope.editThisContact =data;
              });
              $rootScope.states = ['AK', 'AL', 'AR', 'AZ', 'CA', 'CO', 'CT', 'DC', 'DE', 'FL', 'GA', 'HI', 'IA', 'ID', 'IL', 'IN', 'KS', 'KY', 'LA', 'MA', 'MD', 'ME', 'MI', 'MN', 'MO', 'MS', 'MT', 'NC', 'ND', 'NE', 'NH', 'NJ', 'NM', 'NV', 'NY', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VA', 'VT', 'WA', 'WI', 'WV', 'WY'];
          });
        $rootScope.$on('$stateChangeStart', function(e, to){
            if (to.data && to.data.requiresLogin) {
                if (!localStorage.getItem('email')) {
                    e.preventDefault();
                    $state.go("home");
                }
            }
        })
    })

.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');
    $stateProvider .state('tab', {
        url: "/tab",
        abstract: true,
        templateUrl: "templates/tabs.html"
    })
    .state('main', {
        url: "/main",
        abstract: true,
        templateUrl: "templates/main.html"
    })
    .state('home', {
        url: "/home",
        templateUrl: 'templates/home.html',
            data:{
                requiresLogin: false
            }
    })
    .state('911', {
        url: "/911",
        templateUrl: 'templates/911.html',
        data:{
            requiresLogin: false
        }
    })
    .state('editcar', {
        url: "/editcar",
        templateUrl: 'templates/edit_car.html',
        controller: 'EditCarController',
            data:{
                requiresLogin: true
            }
    })
    .state('edit_contact', {
        url: "/edit_contact",
        templateUrl: 'templates/edit_contact.html',
        controller: 'EditContactController',
            data:{
                requiresLogin: true
            }
    })
    .state('tab.accident', {
        url: '/accident',
        data:{
            requiresLogin: true
        },
        views: {
            'tab-accident': {
                templateUrl: 'templates/tab-accident.html',
                controller: 'AlertController'
            }
        }
    })
    .state('tab.alert', {
        url: '/alert',
        data:{
            requiresLogin: true
        },
        views: {
            'tab-alert': {
                templateUrl: 'templates/tab-alert.html',
                controller: 'AlertController'
            }
        }
    })
    .state('tab.contacts', {
        url: '/contacts',
        data:{
            requiresLogin: true
        },
        views: {
            'tab-contacts': {
                templateUrl: 'templates/tab-contacts.html',
                controller: 'ContactController'
            }
        }
    })
    .state('tab.cars', {
        url: '/cars',
        data:{
            requiresLogin: true
        },
        views: {
            'tab-cars': {
                templateUrl: 'templates/tab-cars.html',
                controller: 'CarsController'
            }
        }
    })
    .state('tab.settings', {
        url: '/settings',
        data:{
            requiresLogin: true
        },
        views: {
            'tab-settings': {
                templateUrl: 'templates/tab-settings.html',
                controller: 'SettingsController'
            }
        }
    })
    .state('main.tour', {
        url: '/tour',
        data:{
            requiresLogin: false
        },
        views: {
            'main-tour': {
                templateUrl: 'templates/main-tour.html'
            }
        }
    })
});
