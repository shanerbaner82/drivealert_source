angular.module('starter').controller('AccountController', function($scope, $http, $state){

 $scope.logout = function(){
     window.localStorage.removeItem('email');
     $state.go("home");
 }
});
